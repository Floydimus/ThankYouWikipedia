# Table of contents:
- [#ThankYouWikipedia](#thankyouwikipedia)  
- [Tweet format](#tweet-format)
- [How to use](#how-to-use)  
- [Co-founders](#co-founders)  
<a name="headers"/>

## #ThankYouWikipedia:
One of the largest free information sourcing media for decades, Wikipedia has been a liberal space that is firmly rooted in knowledge for all.

Here's a space to celebrate the largest, most accessible encyclopaedia and more importantly, the spirit of spreading and ideas.

Follow our Twitter to be updated with interesting Wiki articles as we encourage you to contribute quality content to this free knowledge base.

This Twitter bot created as a tribute to, once configured, periodically tweets the link to random articles from wikipedia.

To explore and support, follow us at [@ThankYouWiki](https://twitter.com/ThankYouWiki)



## Tweet format:
We follow the below format:

```
Article name (Wikipedia link)

#ThankYouWikipedia
```


## How to use:
You'll need to install two python packages from pip:

```
pip install python-twitter apscheduler
```

Now you satisfy the requirements, you need to set up the API keys, and the time delay. Generate the API keys
from twitter and put them in their corresponding places in the config. Also, choose a time delay.


## Co-founders:
- Programmed by [Jacob Garby](https://github.com/j4cobgarby) 🇬🇧
- Re-configured by [TheAbbie](https://theabbie.github.io/) 🇮🇳
- Original idea by and maintainer of this repository [Floydimus](https://www.floydimus.prismo.net/)
